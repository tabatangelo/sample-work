## Admin Page UI Refresh
#### Old Manage Campaign Page
![New Manage Campaigns](https://www.dropbox.com/s/pvrf3902t2aq0kn/old_mamange_campaign.png?raw=true)
#### Old Search Campaign Page
![Old Search Campaign](https://www.dropbox.com/s/qgq7tbn54sjmus8/Search%20Campaign%20.png?raw=true)
#### New Manage Campaign Page
![New Manage Campaigns](https://www.dropbox.com/s/d4w8rchecui383z/New%20Campaign%20Manager.png?raw=true)
#### Problem
Update the look of campaign administration pages. The landing page consisted of links to 4 functions:

+ Display Campaigns
+ Create Campaigns
+ Search Campaigns
+ Campaign Report

#### Resolution
Updated the UI to be consistent with current branding utilizing xhtml, css and custom JSF components.
Improved User Experience by integrating multiple functionality into one page. Campaigns were displayed on landing page and a search filter was added eliminating the need for two of the previous pages. Links to other two functions were included as buttons within the dashboard.
